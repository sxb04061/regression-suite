import os
import re
import time
import logging
import pexpect
import openpyxl
import subprocess
from datetime import datetime
from selenium import webdriver

from SupportScript.SetGetValues import set_get



# global log object for common functions file
log = logging.getLogger(__name__)


def return_geckodriver_path():
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # print(base_dir)

    gecko_executable_path = os.path.join(base_dir, "SeleniumDriver/Geckodriver/geckodriver")
    log.info("Gecko driver path :\n{}".format(gecko_executable_path))
    return gecko_executable_path

def return_chromedriver_path():
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # print(base_dir)

    chrome_executable_path = os.path.join(base_dir, "SeleniumDriver/chromedriver/chromedriver")
    log.info("Chrome driver path :\n{}".format(chrome_executable_path))
    return chrome_executable_path


def login_ui_method(driver):

    # 1. Open the login page - ""
    igp_ui_url = set_get.get_url()
    driver.get(igp_ui_url)
    driver.maximize_window()
    log.info("Login to igp ui url :\n{}".format(igp_ui_url))

    # 2. Enter the bitbucket_email and press continue
    driver.refresh()
    time.sleep(5)
    username = driver.find_element_by_id("user")
    username.clear()
    username.send_keys(set_get.get_username())

    user_password = set_get.get_password()
    val = password_checker(user_password)
    if val:
        print("Password validation complete")
    else:
        return False

    password = driver.find_element_by_xpath("//input[@id='password' and @class='form-control' and @name='password']")

    password.clear()
    time.sleep(5)
    print("Password :", user_password)
    password.send_keys(user_password)

    time.sleep(2)
    print("Clicking login button")
    login_button = driver.find_element_by_id("btnLogin")
    login_button.click()

    time.sleep(3)
    print(driver.title)
    if driver.title == "Overview - IGP":
        print("Login successful with user input")
        return True

    else:
        print("Login failed with user password")
        print("Trying to login with default username and password")
        driver.refresh()
        password = driver.find_element_by_id("password")
        # print("Clearing password field")
        password.clear()
        password.send_keys(set_get.get_default_password())
        # #print("default password :password")
        time.sleep(2)
        login_button = driver.find_element_by_id("btnLogin")
        login_button.click()
        # print("Current url :", driver.current_url)
        # print("Expected url :", (igp_ui_url + '#device/access'))
        if driver.current_url == igp_ui_url + '#device/access':
            print("Login with default password successful.")
        else:
            print("Login with default password unsuccessful")
            driver.close()
            return False

    print("Successfully login", driver.current_url)
    log.info("Successfully login :{}".format(driver.current_url))

    time.sleep(3)
    change_password = driver.find_element_by_id("password")
    change_password.clear()
    # print("user password :", user_password)
    change_password.send_keys(user_password)

    confirm_password = driver.find_element_by_id("confirmPassword")
    confirm_password.send_keys(user_password)

    apply_changes = driver.find_element_by_id("saveBtn")
    apply_changes.click()

    print("Your password has been set to user password :{}".format(user_password))
    log.info("Your password has been set to user password :{}".format(user_password))

    # Validation of password change by re-login
    time.sleep(5)
    changed_password = driver.find_element_by_id("password")
    # print("user password :", user_password)
    changed_password.send_keys(user_password)

    time.sleep(2)
    login_button = driver.find_element_by_id("btnLogin")
    login_button.click()

    time.sleep(2)
    wanIp = driver.find_element_by_id("wanIp").text
    print("Your wan Ip is :{}".format(wanIp))
    log.info("Your wan Ip is :{}".format(wanIp))

    return True


def password_checker(password):
    """

    :return:
    """

    special_symbol = ["$", "@", "#", "%", "*", "(", ")", "!", "&", "_", "-"]
    val = True
    if len(password) < 6:
        print("Length should be at least 6")
        val = False

    if len(password) > 20:
        print("Length should not be greater than 8")
        val = False

    if not any(char.isdigit() for char in password):
        print("Password should have at least one numeral")
        val = False

    if not any(char.isupper() for char in password):
        print("Password should have at least one uppercase letter")
        val = False

    if not any(char.islower() for char in password):
        print("Password should have at least one lowercase letter")
        val = False

    if not any(char in special_symbol for char in password):
        print("Password should have at least one special letter [$@#%]")
        val = False
    if val:
        return val

def click_apply_button(driver):
    """

    :return:
    """
    # 1. click on appy button to save configurations
    apply_chg_button = driver.find_element_by_xpath("//button[@id='saveBtn' and @class='btn btn-primary psft-btn' and @type='submit']")
    apply_chg_button.click()


def subnet_based_on_wan(wanip):
    """

    :param wanip:
    :return:
    """
    first_octet = int(wanip.split(".")[0])
    subnets = {"A": "255.0.0.0", "B": "255.255.0.0", "C": "255.255.255.0", "D": "255.255.255.240"}

    # compare the first octet value and return class A, B, C subnet mask
    if 126 >= first_octet >= 0:
        return subnets["A"]
    if 192 > first_octet >= 128:
        return subnets["B"]
    if 224 > first_octet >= 192:
        return subnets["C"]
    if 240 > first_octet >= 224:
        return subnets["D"]
    else:
        print("Please enter a ip from range expect ip starting 127 or > 240")
        return None

def backend_file_content(filename):
    """

    :param filename:
    :return:
    """
    # Access super user
    child_process = pexpect.spawn("sudo su")
    child_process.expect('[sudo].*')
    child_process.sendline("{}\n".format(set_get.get_su()))

    # User input for directory path
    dockerscript_path = set_get.get_dockerpath()
    os.chdir(dockerscript_path)

    # Generate Docker container ID and name
    cmd_output = subprocess.getoutput("docker ps").split("\n")
    split_values = cmd_output[1].split(" ")

    # Establish WAN network connection using Docker container name
    child_process.sendline("docker network connect wanintf {}\n".format(split_values[-1]))

    # Access comparison address from xml file using Docker container ID
    filecontent = subprocess.getoutput("docker exec {} cat /psft/configs/{}\n".format(split_values[0], filename)).split(
        "\n")

    return filecontent

def ip_validation(ip):
    """

    :param ip:
    :return:
    """

    status = re.match(
        '^((([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.)(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.){2}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-4]))$',
        ip)
    if status.group():
        print("IP validation success")
        return True
    else:
        print("IP validation Failed")
        return False


def results_to_excel(fid, tcid, status):
    """

    :return:
    """

    result_filename = "TestResults_{}.xlsx".format(datetime.now().date().strftime("%d-%m-%Y"))
    base_driectory = set_get.get_base_directory()
    result_filepath = os.path.join(base_driectory, "Logs/{}".format(result_filename))
    # First check whether this file exists in logs or not, if not create else search test id and add pass/fail
    if os.path.isfile(result_filepath):
        # print("Test result file exists, adding test status to file")
        log.info("Test result file exists, adding test status to file")
        pass
    else:
        print("Creating Test result file and adding test status to file")
        log.info("Creating Test result file and adding test status to file")
        headers = ["Feature ID", "Test Case ID", "Total Pass", "Total Fail", "Total Runs"]
        workbook = openpyxl.Workbook()
        sheet = workbook.active

        for col in range(1, 6):
            c = sheet.cell(row=1, column=col)
            c.value = headers[col-1]

        workbook.save(result_filepath)

    # Open the workbook again and add the test case values
    workbook = openpyxl.load_workbook(result_filepath)
    sheet = workbook.active
    max_columns = sheet.max_column
    max_rows = sheet.max_row

    not_found = -1

    for r in range(1, max_rows+1):
        c1 = sheet.cell(row=r, column=1).value
        c2 = sheet.cell(row=r, column=2).value
        if c1 == fid and c2 == tcid:
            if status:
                sheet.cell(row=r, column=3).value = sheet.cell(row=r, column=3).value + 1
                # sheet.cell(row=r, column=4).value = 0
                sheet.cell(row=r, column=5).value = sheet.cell(row=r, column=5).value + 1
            else:
                # sheet.cell(row=r, column=3).value = 0
                sheet.cell(row=r, column=4).value = sheet.cell(row=r, column=4).value + 1
                sheet.cell(row=r, column=5).value = sheet.cell(row=r, column=5).value + 1
            not_found = 0
            break

    if not_found != 0:
        if status:
            sheet.append((fid, tcid, 1, 0, 1))
        else:
            sheet.append((fid, tcid, 0, 1, 1))

    workbook.save(result_filepath)




