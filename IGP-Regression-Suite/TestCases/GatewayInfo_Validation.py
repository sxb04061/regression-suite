import re
import os
import time
import logging
import pexpect
import subprocess
from selenium import webdriver
from Common import CommonFunctions as cf
from selenium.common.exceptions import *

class TestGateWayName:

    def __init__(self):
        self.log = logging.getLogger(__name__)
        self.exec_path = cf.return_chromedriver_path()

        # Create a webdriver object
        self.driver = webdriver.Chrome(executable_path=self.exec_path)
        # Login the IGP-UI
        self.login_status = cf.login_ui_method(self.driver)
        if self.login_status:
            print("Starting Gateway name config validation")
            self.log.info("Starting Gateway name config validation")
        else:
            print("Exiting Gateway name config validation")
            self.log.info("Test Failed, Exiting Gateway name config validation")


    def test_gatewaytab(self):
        """

        :return:
        """
        try:
            print("Test - Gateway tab")
            self.log.info("Test - Gateway tab")
            gateway_tab = self.driver.find_element_by_xpath("//ul[@class='list-inline hidden-xs']//a[@href='#device/general']")
            gateway_tab.click()
            self.log.info("Clicked Gateway tab")
            time.sleep(2)
            page_title = self.driver.title
            if page_title == "Gateway - IGP":
                print("Test passed, Page title matched :{}".format(page_title))
                self.log.info("Test passed, Page title matched :{}".format(page_title))
                return True
            else:
                print("Test failed, Page Title doesn't match :{}".format(page_title))
                self.log.debug("Test failed, Page Title doesn't match :{}".format(page_title))
                return False
        except Exception as ex:
            print("Test failed, Found Exception :{}".format(ex))
            self.log.critical("Test failed, Found Exception :{}".format(ex))
            return False

    def test_change_gateway_name(self):
        """

        :return:
        """

        try:
            print("Test - Gateway Name")
            self.log.info("Test - Gateway Name")

            gateway_name = "IGP-SIMULATOR"

            gateway = self.driver.find_element_by_xpath("//input[@id='gateway-name']")
            gateway.clear()
            gateway.send_keys(gateway_name)
            self.log.info("Added gateway name :{}".format(gateway_name))
            cf.click_apply_button(self.driver)

            self.driver.get("http://127.0.0.1:8887/#device/overview")
            time.sleep(5)
            validate_gt_name = self.driver.find_elements_by_xpath("//div[@class='col-xs-7']")

            if gateway_name == validate_gt_name[1].text:
                print("Test passed, Gateway name matches "
                      "with overview page gateway name :{}".format(validate_gt_name[1].text))
                self.log.info("Test passed, Gateway name matches"
                              " with overview page gateway name :{}".format(validate_gt_name[1].text))
                return True
            else:
                print("Test failed, Gateway name:{} does not matches"
                      " with overview page gateway name".format(validate_gt_name[1].text))
                print("Test failed, Gateway name:{} does not matches"
                      " with overview page gateway name".format(validate_gt_name[1].text))
                return False
        except Exception as ex:
            print("Found Exception :{}".format(ex))
            self.log.critical("Test failed, Found Exception :{}".format(ex))
            return False

    def test_change_description(self):
        """

        :return:
        """
        try:
            print("Test - Gateway Description")
            self.log.info("Test - Gateway Description")

            gateway_des = "IGP-BOX"

            gateway_tab = self.driver.find_element_by_xpath(
                "//ul[@class='list-inline hidden-xs']//a[@href='#device/general']")
            gateway_tab.click()
            time.sleep(2)

            desc_text_input = self.driver.find_element_by_xpath("//input[@id='gateway-description']")
            desc_text_input.clear()
            desc_text_input.send_keys(gateway_des)
            self.log.info("Added description :{}".format(gateway_des))

            time.sleep(2)
            cf.click_apply_button(self.driver)

            self.driver.get("http://127.0.0.1:8887/#device/overview")
            self.log.info("Go to overview page")
            time.sleep(5)

            subtitle = self.driver.find_element_by_xpath("//p[@id='subtitle']")

            if gateway_des == subtitle.text:
                print("Test passed, User description matches"
                      " with subtitle on overview page :{}".format(subtitle.text))
                self.log.info("Test passed, User description matches"
                              " with subtitle on overview page :{}".format(subtitle.text))
                return True
            else:
                print("Test failed, User description:{} doesn't matches"
                      " with subtitle on overview page".format(subtitle.text))
                self.log.debug("Test failed, User description :{} doesn't matches"
                               " with subtitle on overview page".format(subtitle.text))
                return False

        except Exception as ex:
            print("Found Exception :{}".format(ex))
            self.log.critical("Test failed, Found Exception :{}".format(ex))
            return False

    def test_address(self):
        """

        :return:
        """
        try:
            print("Test - Gateway Address")
            self.log.info("Test - Gateway Address")

            gateway_address_input = "IGP-USA"

            gateway_tab = self.driver.find_element_by_xpath(
                "//ul[@class='list-inline hidden-xs']//a[@href='#device/general']")
            gateway_tab.click()
            time.sleep(5)

            add_text_box = self.driver.find_element_by_xpath("//textarea[@id='address1']")
            add_text_box.clear()
            add_text_box.send_keys(gateway_address_input)
            self.log.info("Added description :{}".format(gateway_address_input))

            time.sleep(2)
            cf.click_apply_button(self.driver)

            filecontent = cf.backend_file_content("customer.xml")

            # Comparison and return
            flag = 0
            for lines in filecontent:
                if "<address-line1>{}</address-line1>".format(gateway_address_input) in lines:
                    flag = 1
                    break
                else:
                    flag = -1

            if flag == 1:
                print("Address field:{} verified from backend xml file".format(gateway_address_input))
                self.log.info("Address field:{} verified from backend xml file".format(gateway_address_input))
                return True
            else:
                print("Address field:{} not updated in backend xml file".format(gateway_address_input))
                self.log.debug("Address field:{} not updated in backend xml file".format(gateway_address_input))
                return False

        except Exception as ex:
            print("Found Exception :{}".format(ex))
            self.log.critical("Test failed, Found Exception :{}".format(ex))
            return False





