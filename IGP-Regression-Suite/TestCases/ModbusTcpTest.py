import os
import time
import pytest
import subprocess
import pexpect
import logging
from Common import CommonFunctions as cf
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import *


class TestModbusAgent:

    def __init__(self):
        self.log = logging.getLogger(__name__)
        self.exec_path = cf.return_geckodriver_path()

        # Create a webdriver object
        self.driver = webdriver.Firefox(executable_path=self.exec_path)

        # Login the IGP-UI
        self.login_status = cf.login_ui_method(self.driver)
        if self.login_status:
            print("Starting Modbus TCP agent config validation")
            self.log.info("Starting Modbus TCP agent config validation")
        else:
            print("Login failed, Exiting Modbus TCP agent config validation")
            self.log.info("Login failed, Exiting Modbus TCP agent config validation")

    def test_modbus_agent_disable(self):
        """
        Disable modbus agent status
        :return:
        """
        try:

            modbus_status = 1

            # 1. After login click on the gateway tab

            gateway_tab = self.driver.find_element_by_xpath(
                "//ul[@class='list-inline hidden-xs']//a[@href='#device/general']")
            gateway_tab.click()

            time.sleep(3)

            # 2. First disable the DDNS feature
            self.driver.execute_script("arguments[0].scrollIntoView();",
                                       self.driver.find_element_by_xpath("//legend[contains(text(), 'Cellular Interface')]"))
            time.sleep(2)

            modbus_options = Select(self.driver.find_element_by_xpath("//select[@id='modbus_agent_enable']"))

            for options in modbus_options.options:
                if options.text == "Disabled":
                    modbus_options.select_by_visible_text(options.text)
                    modbus_status = 0

            # 4. Click on the apply changes button
            time.sleep(3)
            cf.click_apply_button(self.driver)

            # 5. Validate on the backend
            filecontent = cf.backend_file_content("protocols.xml")
            flag = 0
            for lines in filecontent:
                if "<enable>{}</enable>".format(modbus_status) in lines:
                    print("Test Passed, Modbus agent is disabled and verified from backend")
                    self.log.info("Test Passed, Modbus agent is disabled and verified from backend")
                    return True
                else:
                    flag = -1
                    continue
            if flag == -1:
                print("Test Failed, Modbus agent is not disabled and verified from backend")
                self.log.debug("Test Failed, Modbus agent is not disabled and verified from backend")
                return False

        except StaleElementReferenceException as stale_ex:
            print("Found StaleElementReferenceException :{}".format(stale_ex))
            return False

        except NoSuchElementException as no_ele_ex:
            print("Found NoSuchElement Exception :{}".format(no_ele_ex))
            return False

        except Exception as ex:
            print("Found Exception :{}".format(ex))
            return False

    def test_modbus_agent_enable(self):
        """

        :return:
        """

        try:
            self.driver.execute_script("arguments[0].scrollIntoView();",
                                       self.driver.find_element_by_xpath("//legend[contains(text(), 'Cellular Interface')]"))
            time.sleep(2)

            modbus_status = 1
            modbus_options = Select(self.driver.find_element_by_xpath("//select[@id='modbus_agent_enable']"))

            time.sleep(15)
            for options in modbus_options.options:
                if options.text == "Enabled":
                    modbus_options.select_by_visible_text(options.text)
                    modbus_status = 1

            # 7. Click on the apply changes button
            time.sleep(3)
            cf.click_apply_button(self.driver)

            # 8. Validate on the backend
            filecontent = cf.backend_file_content("protocols.xml")
            flag = 0
            for lines in filecontent:
                if "<enable>{}</enable>".format(modbus_status) in lines:
                    print("Test Passed, Modbus agent is enabled and verified from backend")
                    self.log.info("Test Passed, Modbus agent is enabled and verified from backend")
                    return True
                else:
                    flag = -1
                    continue
            if flag == -1:
                print("Test Failed, Modbus agent is not enabled and verified from backend")
                self.log.debug("Test Failed, Modbus agent is not enabled and verified from backend")
                return False


        except StaleElementReferenceException as stale_ex:
            print("Found StaleElementReferenceException :{}".format(stale_ex))
            return False

        except NoSuchElementException as no_ele_ex:
            print("Found NoSuchElement Exception :{}".format(no_ele_ex))
            return False

        except Exception as ex:
            print("Found Exception :{}".format(ex))
            return False

    def test_modbus_agent_listenport(self):
        """

        :return:
        """
        try:
            self.driver.execute_script("arguments[0].scrollIntoView();",
                                       self.driver.find_element_by_xpath("//legend[contains(text(), 'Cellular Interface')]"))
            time.sleep(2)

            # 9. Now enter value in listen port and validate from backend
            listen_port = self.driver.find_element_by_xpath("//*[@id='modbus_agent_port']")

            # 10. enter value -1 and validate
            test_values = [-1, 65536, 0, 100]

            for values in test_values:
                time.sleep(10)
                listen_port.clear()
                listen_port.send_keys(values)

                try:
                    help_msg = self.driver.find_element_by_xpath("//p[contains(text(), 'Must be a number between 0 and 65535')]")
                    print("{} is not accepted as a valid input. {}".format(values, help_msg.text))
                    self.log.info("{} is not accepted as a valid input. {}".format(values, help_msg.text))
                except NoSuchElementException:
                    # 11. Click on the apply button if there is no exception

                    time.sleep(5)
                    cf.click_apply_button(self.driver)

                    # 11. Validate on the backend
                    time.sleep(10)
                    filecontent = cf.backend_file_content("protocols.xml")
                    flag = 0
                    for lines in filecontent:
                        if "<port>{}</port>".format(values) in lines:
                            print("Test passed, Listen port:{} is validated and verified from backend".format(values))
                            self.log.info("Test passed, Listen port:{} is validated and verified from backend".format(values))
                            return True
                        else:
                            flag = -1
                            continue
                    if flag == -1:
                        print("Test failed, Listen port:{} is not validated from backend".format(values))
                        self.log.debug("Test failed, Listen port:{} is not validated from backend".format(values))
                        return False

        except StaleElementReferenceException as stale_ex:
            print("Found StaleElementReferenceException :{}".format(stale_ex))
            return False

        except NoSuchElementException as no_ele_ex:
            print("Found NoSuchElement Exception :{}".format(no_ele_ex))
            return False

        except Exception as ex:
            print("Found Exception :{}".format(ex))
            return False

def main():
    """
    This main function will be called from the MainEngine file.
    This function
    :return:
    """

    # Create test object
    test_obj = TestModbusAgent()
    fid = "IGP_MODBUS"
    tcid_disable = "Modbus_Disable"
    tcid_enable = "Modbus_Enable"
    tcid_listenport = "Modbus_ListenPort"

    # 1. Run first test and pass the results to create excel report
    tc1_status = test_obj.test_modbus_agent_disable()
    cf.results_to_excel(fid, tcid_disable, tc1_status)

    # 2. Run Second test and pass the results to create excel report
    tc2_status = test_obj.test_modbus_agent_enable()
    cf.results_to_excel(fid, tcid_enable, tc2_status)

    # 3. Run Third test and pass the results to create excel report
    tc3_status = test_obj.test_modbus_agent_listenport()
    cf.results_to_excel(fid, tcid_listenport, tc3_status)

    # Close the driver object at the end of the test
    test_obj.driver.close()
    test_obj.driver.quit()

