import re
import os
import time
import logging
import argparse
import importlib
from datetime import datetime
from SupportScript.SetGetValues import set_get
from SupportScript.Logging import logmethod


class MainTest:

    def __init__(self, args, test_name):
        """
        Set class local variables and set other variables that can be called anywhere in tests after setting
        them.
        """
        # self.testname = args.test_name
        self.testname = test_name
        set_get.set_username(args.username)
        set_get.set_password(args.password)
        set_get.set_url(args.url)
        set_get.set_su(args.suser)
        set_get.set_dockerpath(args.dockerpath)


    def import_test_case(self):
        """

        :return:
        """

        # 1. Import the test case passed by user as an input
        module = importlib.import_module("TestCases.{}".format(self.testname))

        # 2. Call the main function of the imported module
        status = module.main()

        # 3. Return the test results so that it can be appended to the file
        return status


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--url", help="Enter the url to login IGP UI")
    parser.add_argument("-uname", "--username", help="Enter the username to login")
    parser.add_argument("-p", "--password", help="Enter the password to login")
    parser.add_argument("-su", "--suser", help="Enter the super user password")
    parser.add_argument("-dspath", "--dockerpath", help="Enter the docker script path")

    # Take all the arguments and pass them to the class constructor
    args = parser.parse_args()

    # tests = []
    tests = ["GatewayInfo_Validation"]

    for name in tests:
        # Create the object for the main engine file
        # log_obj = logmethod("{}_{}".format(name, datetime.now().strftime("%d-%m-%Y_%H:%M:%S")))
        log_obj = logmethod("{}_{}".format("TestRun", datetime.now().strftime("%d-%m-%Y_%H:%M:%S")))
        log_obj.info("Passing arguments :{}".format(args))
        start_time = datetime.now()
        date = datetime.now().date().strftime("%d-%m-%Y")
        print("--------------Start TestCase :{}-----------------".format(name))
        main_obj = MainTest(args, test_name=name)
        main_obj.import_test_case()
        end_time = datetime.now()
        print("Time taken :", end_time - start_time)
        print("--------------End TestCase :{}-----------------\n".format(name))
        time.sleep(5)








