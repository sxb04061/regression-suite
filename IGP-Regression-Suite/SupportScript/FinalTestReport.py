import pandas as pd
import time
import matplotlib.pyplot as plt
import openpyxl
from openpyxl.chart import Reference, BarChart3D, Series


filepath = "/home/shubhankar/IGP-CODES/IGP-Regression-Suite/Logs/TestResults_16-09-2020.xlsx"

df = pd.read_excel(filepath)
groups_dict = df.groupby("Feature ID").groups


wb = openpyxl.load_workbook(filepath)
ws = wb.active
mx_col =ws.max_column
mx_row =ws.max_row
print(mx_col)
print(mx_row)


data = Reference(ws, min_row=2, min_col=3, max_row=7, max_col=4)
title = Reference(ws, min_row=2, min_col=2, max_row=7)
print(title)
chart = BarChart3D()
chart.title = "IGP_Gateway"
chart.add_data(data=data, titles_from_data=True)
chart.set_categories(title)
ws.add_chart(chart, "H2")
wb.save(filepath)



"""for group_name, l in groups_dict.items():
    if group_name == "IGP_Gateway":
        print(group_name, l[0]+2, l[-1]+2)
        data = Reference(ws, min_row=l[0]+2, min_col=3, max_row=l[-1]+2, max_col=4)
        title = Reference(ws, min_row=l[0]+2, min_col=2, max_row=l[-1]+2)
        chart = BarChart()
        chart.title = group_name
        chart.add_data(data=data, titles_from_data=True)
        chart.set_categories(title)

        ws.add_chart(chart, "H{}".format(l[0]+2))
        wb.save(filepath)"""

