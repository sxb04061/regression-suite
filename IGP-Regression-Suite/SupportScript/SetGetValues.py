import os


class SetGet:

    def __init__(self):
        """

        """
        self.username = "admin"
        self.default_password = "password"
        self.user_password = "Test@123"
        self.url = "http://127.0.0.1:8887"
        self.su = "root"
        self.dockerpath = ""


    def set_username(self, username):
        """
        Set the username entered by the user
        :return:
        """
        self.username = username
        print("Username is set :", self.username)

    def get_username(self):
        """
        Return the username
        :return:
        """
        return self.username

    def set_password(self, password):
        """
        Set the username entered by the user
        :return:
        """
        self.user_password = password
        print("User password is set :", self.user_password)

    def get_password(self):
        """
        Return the username
        :return:
        """
        return self.user_password

    def get_default_password(self):
        """
        Return the default password
        :return:
        """
        return self.default_password

    def set_url(self, url):
        """
        Set the username entered by the user
        :return:
        """
        self.url = url
        print("Url set is :", self.url)

    def get_url(self):
        """
        Return the username
        :return:
        """
        return self.url

    def get_base_directory(self):
        """
        Return the base directory path
        :return:
        """

        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # print("Base directory :", base_dir)
        return base_dir

    def set_su(self, su):
        """
        Set the uper user password entered by the user
        :return:
        """
        self.su = su
        print("Super user is set to:", self.su)

    def get_su(self):
        """
        Return the super user password
        :return:
        """
        return self.su

    def set_dockerpath(self, path):
        """
        Set the dockerpath entered by the user
        :return:
        """
        self.dockerpath = path
        print("docker path is set to:", self.dockerpath)

    def get_dockerpath(self):
        """
        Return the dockerpath
        :return:
        """
        return self.dockerpath

# Create an object of the class
set_get = SetGet()

