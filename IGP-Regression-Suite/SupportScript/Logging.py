import os
import sys
import logging
import logging.config


def logmethod(filename):
    logger = logging.getLogger(filename)
    logdir = os.path.join(os.getcwd(), "Logs/TestLogs")

    # Creating and adding the file handler
    logging.basicConfig(filename='{}/{}.log'.format(logdir, filename),
                        filemode='w',
                        level=logging.INFO,
                        datefmt='%d-%m-%Y %H:%M:%S',
                        format='%(asctime)s - %(levelname)s - %(message)s')
    logger.info("Logger object successfully created!!!!")
    return logger


"""
logging.basicConfig(filename='{}/{}.log'.format(logdir, filename),
                filemode='a',
                level=logging.INFO,
                datefmt='%d-%m-%Y %H:%M:%S',
                format='%(asctime)s - %(levelname)s - %(message)s')

log_obj = logging.getLogger()
log_obj.info("Logger object successfully created!!!!")
return log_obj
"""